#!/bin/env bash

xvfb-run --server-args="-screen 0, 1280x1200x24" \
    cutycapt --min-width=${WIDTH} \
    --min-height=${HEIGHT} \
    --url="${URL}" \
    --max-wait=5000 \
    --out=/tmp/web.png &>/dev/null

convert /tmp/web.png \
    -depth 4 \
    -colorspace gray \
    -crop ${WIDTH}x${HEIGHT}-0-0 \
    /tmp/out.png

cat /tmp/out.png | base64 -w 0 | tr -d '\n'

